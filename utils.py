import random
import pandas
import numpy as np
import ast
from xml.etree.ElementTree import Element
import re

def random_subset(iterator, k):
    result = []
    n = 0
    for item in iterator:
        n += 1
        if len(result) < k:
            result.append(item)
        else:
            s = int(random.random() * n)
            if s < k:
                result[s] = item
    return result

def get_kb(kb_path):
    kb = pandas.read_csv(kb_path, dtype=np.dtype('object')).set_index('appId')
    filtered_db = kb.loc[(kb.active == 'TRUE') & (kb.domainList is not None)]
    filtered_db['domainList'] = filtered_db['domainList'].apply(ast.literal_eval)
    return filtered_db

def random_ips(num_ips, ip_startrange = '10.0'):
    ips = []
    new_ip = '{}.{}.{}'.format(ip_startrange, random.randint(1, 10), random.randint(1, 254))
    for i in xrange(num_ips):
        while new_ip in ips:
            new_ip = '{}.{}.{}'.format(ip_startrange, random.randint(1, 10), random.randint(1, 254))
        ips.append(new_ip)
    return ips

def multiple_replace(dict, text):
  regex = re.compile("(%s)" % "|".join(map(re.escape, dict.keys())))
  return regex.sub(lambda mo: dict[mo.string[mo.start():mo.end()]], text)


def random_linear_list(minint, maxint, numvalues, randomize=False, acending=True):
    a = []
    for i in xrange(numvalues):
        a.append(random.randint(minint, maxint))
    if not randomize:
        if acending:
            a.sort(reverse=True)
        else:
            a.sort()
    return a

def dict_to_xml(tag, d):
    '''
    Turn a simple dict of key/value pairs into XML
    '''
    elem = Element(tag)
    for key, val in d.items():
        child = Element(key)
        if type(val) == dict:
            for k, v in val["attr"].items():
                child.attrib[k] = v
            val = val["value"]
        if val:
            child.text = str(val)
        elem.append(child)
    return elem


def get_services(services_path):
    services = pandas.read_csv(services_path, dtype=np.dtype('object')).set_index('appId')
    return services['story'].to_dict()