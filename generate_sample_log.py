import json
from collections import OrderedDict
from utils import get_kb, get_services
import os
import argparse
from Appliance import ApplianceEntity
from Story import StoryEntity
from LogGenerator import CsvLog, OrderedLineLog, KeyValueLog, XmlLog


def get_conf(file_path):
    return json.load(open(file_path), object_pairs_hook=OrderedDict)

BASE_DIR = os.path.dirname(__file__)
CONF_PATH = os.path.join(BASE_DIR, 'config/main_conf.json')
conf = get_conf(CONF_PATH)
KB_PATH = conf["kb_path"]
APLNCES_DIR = conf["appliances_dir"]
APLNCS_DICT = get_conf(os.path.join(BASE_DIR, conf["appliances_json_path"]))
SRVCS_PATH = conf["services_path"]
USERS_PATH = conf["users_path"]
STORIES_PATH = conf["stories_path"]




def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-l', '--logtype', dest='log_type', help='the number of the appliance', type=str)
    parser.add_argument('-o', '--out', dest='out_path', help='the path ofthe sample log to be generated', required=False)
    parser.add_argument('-d', '--daysback', dest='days_back', default=10, help='the number of days back for the log generator', required=False, type=int)
    parser.add_argument('-s', '--stories', dest='stories_list', required=False, help='list of all story names to be generated, in this format without spaces: ["story1","story2"]', type=list)
    return parser.parse_args()




def load_all_stories(conf_path, users, stories_list, daysback):
    stories_dict = dict()
    stories = json.load(open(conf_path), object_pairs_hook=OrderedDict)
    if stories_list is None:
        stories_list = stories.keys()
    for story_name in stories_list:
        stories_dict[story_name] = StoryEntity(stories[story_name], users, daysback)
    return stories_dict



def getLog(log_number, output_path, kb):
    appliance_json = get_conf(os.path.join(BASE_DIR, APLNCES_DIR, APLNCS_DICT[log_number] + '.json'))
    aplnc = ApplianceEntity(appliance_json)
    if output_path is None:
        if not(os.path.exists(os.path.join(BASE_DIR, 'outputs'))):
            os.mkdir(os.path.join(BASE_DIR, 'outputs'))
        output_path = os.path.join(BASE_DIR, 'outputs/{}_sample_log.log'.format(aplnc.name))
    if aplnc.format_type == "csv":
        return CsvLog(output_path, aplnc, kb)
    if aplnc.format_type == "orderedline":
        return OrderedLineLog(output_path, aplnc, kb)
    if aplnc.format_type == "keyvalue":
        return KeyValueLog(output_path, aplnc, kb)
    if aplnc.format_type == "xml":
        return XmlLog(output_path, aplnc, kb)




def main():
    args = parse_arguments()
    users = get_conf(os.path.join(BASE_DIR, USERS_PATH))
    stories = load_all_stories(os.path.join(BASE_DIR, STORIES_PATH), users, args.stories_list, args.days_back)
    kb = get_kb(os.path.join(BASE_DIR, KB_PATH))
    services = get_services(os.path.join(BASE_DIR, SRVCS_PATH))
    sample_log = getLog(args.log_type, args.out_path, kb)
    sample_log.generate_log(services, stories)


if __name__ == "__main__":
    main()
