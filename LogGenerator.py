from datetime import date, timedelta
import random
from collections import OrderedDict
import re
from utils import random_linear_list, random_ips, random_subset, dict_to_xml, multiple_replace
import tldextract
from xml.etree.ElementTree import tostring
import ast
import copy

BYTES_FIELDS = ["<bytesReceived>", "<bytesSent>", "<totalBytes>"]

class LogEntity:
    def __init__(self, output, aplnc, kb):
        self.appliance = aplnc
        self.kb = kb
        self.generators = {
            "<datetime>": self.generate_datetime,
            "<time>": self.generate_time,
            "<srcip>": self.generate_src_ip,
            "<dstip>": self.generate_dst_ip,
            "<username>": self.generate_user,
            "<domainName>": self.generate_url,
            "<fullUrl>": self.generate_full_url,
            "<bytesReceived>": self.generate_bytes_recieved,
            "<bytesSent>": self.generate_bytes_sent,
            "<totalBytes>": self.generate_total_bytes,
            "<uriPath>": self.generate_uri_path,
            "<autoNumber>": self.generate_auto_number
        }
        self.date_today = date.today()
        self.file = open(output, 'wb')
        self.auto_number = 0


    def generate_datetime(self):
        if self.appliance.date_as_number:
            return '{}'.format(int((self.current_date - date(1970,1,1)).total_seconds()))
        return self.current_date.strftime(self.appliance.date_pattern)

    def generate_auto_number(self):
        self.auto_number += 1
        return str(self.auto_number)


    def generate_time(self):
        return self.current_date.strftime(self.appliance.time_pattern)

    def generate_total_bytes(self):
        received = self.generate_bytes_recieved()
        sent = self.generate_bytes_sent()
        return received + sent

    def generate_full_url(self):
        return self.current_full_url

    def generate_src_ip(self):
        self.current_src_ip = random.choice(self.daily_ips)
        return self.current_src_ip

    def generate_dst_ip(self):
        ip_list = self.kb.ix[str(self.current_appid)]['ip.v4_addressList']
        if not ip_list:
            self.current_dst_ip = '4.4.4.4'
        else:
            self.current_dst_ip = ip_list[0]
        return self.current_dst_ip

    def generate_user(self):
        self.current_user = self.daily_users.pop()
        return self.current_user

    def generate_uri_path(self):
        return self.current_uri_path

    def generate_url(self):
        return self.current_domain

    def generate_bytes_recieved(self):
        return int(random.random() * self.daily_download_bytes / self.daily_users_num * 2)

    def generate_bytes_sent(self):
        return int(random.random() * self.daily_upload_bytes / self.daily_users_num * 2)

    def get_current_domain(self):
        domain = self.kb.ix[str(self.current_appid)][u'domainList'][0]
        self.current_full_url = domain
        ex = tldextract.extract(domain)
        if not ex.subdomain:
            domain = 'www.' + domain
        if any("<uriPath>" in x for x in self.appliance.fieldslist.values()):
            self.current_domain = domain.split('/')[0]
        else:
            self.current_domain = domain
        self.current_uri_path = '/' + '/'.join(domain.split('/')[1:])

    def get_user_daily_events(self):
        return int(random.random() * self.daily_total_events / self.daily_users_num * 2)

    def generate_field_value(self, raw_field_value, for_transactions, for_doubleline):
        re_str = '(' + '|'.join(self.generators.keys()) + ')'
        if type(raw_field_value) in [dict, OrderedDict]:
            raw_field_value = raw_field_value["value"]
        matches = re.findall(re_str, raw_field_value)
        for m in matches:
            if m in BYTES_FIELDS and for_transactions:
                value = '1'
            elif m == "<username>" and (for_transactions or for_doubleline):
                value = self.current_user
            elif m == "<srcip>" and (for_doubleline or for_transactions):
                value = self.current_src_ip
            elif m == "<dstip>" and (for_doubleline or for_transactions):
                value = self.current_dst_ip
            else:
                value = str(self.generators[m]())
            raw_field_value = re.sub(m, value, raw_field_value)
        return raw_field_value

    def generate_line(self, appliance_fields, for_transactions=False, for_doubleline=False):
        line_builder = OrderedDict()
        for field in appliance_fields.keys():
            if appliance_fields[field] == "":
                continue
            line_builder[field] = self.generate_field_value(appliance_fields[field], for_transactions, for_doubleline)
        return line_builder

    def fix_lines(self, line):
        if self.appliance.replace_strings:
            for fix in self.appliance.replace_strings:
                line = re.sub(fix["subStringToReplace"], fix["subStringToReplaceWith"], line)
        return line

    def generate_doubleline_fieldlist(self):
        new_aplnc_fieldlist = OrderedDict()
        if self.appliance.is_doubleline:
            for k in self.appliance.fieldslist.keys():
                new_aplnc_fieldlist[k] = multiple_replace(self.appliance.doubleline_replaces, self.appliance.fieldslist[k])
        return new_aplnc_fieldlist

    def generate_daily_log(self):
        for i in xrange(len(self.daily_users)):
            line_builder = self.generate_line(self.appliance.fieldslist)
            self.file.write("{}{}".format(self.appliance.line_intro, self.format_line(line_builder)))
            if self.appliance.is_doubleline:
                line_builder = self.generate_line(self.doubleline_fieldlist, for_doubleline=True)
                self.file.write("{}{}".format(self.appliance.line_intro, self.format_line(line_builder)))
            if self.appliance.has_bytes_fields:
                daily_user_events = self.get_user_daily_events()
                for j in xrange(daily_user_events):
                    trans_line_builder = self.generate_line(self.appliance.fieldslist, for_transactions=True)
                    self.file.write("{}{}".format(self.appliance.line_intro, self.format_line(trans_line_builder)))


    def get_download_bytes_list(self):
        if self.current_story.is_random:
            return [int(random.random() * self.current_story.max_random_bytes * self.current_story.daysback)] * self.current_story.daysback
        return random_linear_list(self.current_story.min_random_bytes, self.current_story.max_random_bytes, self.current_story.daysback, acending=self.current_story.is_acending)


    def get_upload_bytes_list(self):
        if self.current_story.is_random:
            return [int(random.random() * self.current_story.max_upload_bytes * self.current_story.daysback)] * self.current_story.daysback
        return random_linear_list(self.current_story.max_upload_bytes/2, self.current_story.max_upload_bytes, self.current_story.daysback, acending=self.current_story.is_acending)

    def handle_ips(self, services, filter=False):
        if filter:
            self.kb = self.kb.loc[(self.kb['ip.v4_addressList'] != '[]')]
            services = {k: v for k, v in services.iteritems() if k in self.kb.index}
        self.kb['ip.v4_addressList'] = self.kb['ip.v4_addressList'].apply(ast.literal_eval)
        return services

    def generate_log(self, services, stories):
        print 'generating {} log...'.format(self.appliance.name)
        self.file.write(self.appliance.intro)
        self.file.flush()
        filter = not(self.appliance.url_based)
        services = self.handle_ips(services, filter)
        self.doubleline_fieldlist = self.generate_doubleline_fieldlist()
        for appid in services.keys():
            self.generate_appid_log(appid, stories[services[appid]])
            self.file.flush()
        self.file.close()
        print 'log written to file {}'.format(self.file.name)


    def generate_appid_log(self, appid, story):
        self.current_appid = appid
        self.get_current_domain()
        self.current_story = story
        current_download_bytes = self.get_download_bytes_list()
        current_upload_bytes = self.get_upload_bytes_list()
        num_users_list = random_linear_list(self.current_story.min_users, self.current_story.max_users, self.current_story.daysback, randomize=self.current_story.is_random, acending=self.current_story.is_acending)
        total_events = random.random() * (self.current_story.max_random_bytes / 100000)
        for i in xrange(self.current_story.daysback):
            print 'generating log: {} - {}'.format(self.current_appid, i)
            self.current_date = self.date_today - timedelta(days=i)
            if self.current_date.isoweekday() in [6,7]:
                continue
            self.daily_download_bytes = random.random() * current_download_bytes[i]
            self.daily_upload_bytes = random.random() * current_upload_bytes[i]
            self.daily_total_events = random.random() * total_events
            self.daily_users_num = num_users_list[i]
            self.daily_users = random_subset(self.current_story.users, self.daily_users_num)
            self.daily_ips = random_ips(self.daily_users_num)
            self.generate_daily_log()

class CsvLog(LogEntity):
    def __init__(self, output, aplnc, kb):
        LogEntity.__init__(self, output, aplnc, kb)

    def format_line(self, fields_json):
        return self.appliance.delimiter.join([str(x) for x in fields_json.values()]) + "\n"


class OrderedLineLog(LogEntity):
    def __init__(self, output, aplnc, kb):
        LogEntity.__init__(self, output, aplnc, kb)

    def format_line(self, fields_json):
        return self.fix_lines(fields_json["line"]) + "\n"

class KeyValueLog(LogEntity):
    def __init__(self, output, aplnc, kb):
        LogEntity.__init__(self, output, aplnc, kb)

    def format_line(self, fields_json):
        return self.fix_lines(self.appliance.delimiter.join(["{}{}{}".format(x, self.appliance.separator, fields_json[x]) for x in fields_json.keys()])) + '\n'

class XmlLog(LogEntity):
    def __init__(self, output, aplnc, kb):
        LogEntity.__init__(self, output, aplnc, kb)

    def format_line(self, fields_json):
        return self.fix_lines(tostring(dict_to_xml(self.appliance.xml_root, fields_json))) + '\n'
