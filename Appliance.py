import json
from collections import OrderedDict


BYTES_FIELDS = ["<bytesReceived>", "<bytesSent>"]

class ApplianceEntity:
    def __init__(self, aplnc_json):
        self.name = aplnc_json['parserName']
        self.format_type = aplnc_json['formatType']
        self.date_pattern = aplnc_json['datePattern']
        self.time_pattern = aplnc_json['timePattern'] if 'timePattern' in aplnc_json.keys() else ""
        self.date_as_number = aplnc_json['dateAsNumber'] if 'dateAsNumber' in aplnc_json.keys() else False
        self.fieldslist = aplnc_json['fields']
        if "advancedCSV" in aplnc_json.keys():
            title = aplnc_json["advancedCSV"]["titlesFields"]
            if title != "":
                title += " "
            self.delimiter = aplnc_json["advancedCSV"]["delimiter"]
            self.fieldstitle = "{}{}\n".format(title, self.delimiter.join(self.fieldslist.keys()))
        elif "advancedKeyValue" in aplnc_json.keys():
            self.fieldstitle = ""
            self.delimiter = aplnc_json["advancedKeyValue"]["delimiter"]
            self.separator = aplnc_json["advancedKeyValue"]["keyValueSeparator"]
        else:
            self.fieldstitle = ""
        self.has_bytes_fields = aplnc_json["hasBytesFields"] if "hasBytesFields" in aplnc_json.keys() else True
        self.intro = (aplnc_json['intro'] if 'intro' in aplnc_json.keys() else '') + self.fieldstitle
        self.url_based = aplnc_json['ipOrURL'] == 'url'
        self.line_intro = aplnc_json['preLine'] if 'preLine' in aplnc_json.keys() else ""
        self.xml_root = aplnc_json['xmlRoot'] if 'xmlRoot' in aplnc_json.keys() else ""
        self.replace_strings = aplnc_json['replaceStrings'] if 'replaceStrings' in aplnc_json.keys() else False
        self.is_doubleline = aplnc_json['isDoubleLine'] if 'isDoubleLine' in aplnc_json.keys() else False
        self.doubleline_replaces = aplnc_json['doubleLineReplaceStrings'] if 'doubleLineReplaceStrings' in aplnc_json.keys() else False
