class StoryEntity:
    def __init__(self, story_json, users_json, daysback):
        self.max_random_bytes = story_json["maxRandomBytes"]
        self.max_upload_bytes = self.max_random_bytes / 5
        self.is_random = story_json["type"] == "random"
        self.is_acending = story_json["type"] == "acending"
        self.daysback = daysback
        if not self.is_random:
            self.min_random_bytes = story_json["minRandomBytes"]
            if daysback <= 10: self.daysback = 30
        self.get_users(story_json["usersType"], users_json)
        self.max_users = story_json["maxUsers"]
        self.min_users = story_json["minUsers"]


    def get_users(self, users_type, users_json):
        if users_type in users_json.keys():
            self.users = users_json[users_type]
        else:
            self.users = []
            for x in users_json.keys():
                self.users += users_json[x]